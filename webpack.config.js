const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const SassProccessor = new ExtractTextPlugin({
  filename: "styles.css",
});
module.exports = (env = {}) => {
  return {
    context: __dirname,
    entry: './src/app.js',
    devtool: 'eval',
    devServer: {
      contentBase: './dist',
      port: 3000
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new HtmlWebpackPlugin({
        template: 'src/index.html'
      }),
      SassProccessor
    ],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: env.prod ?
            SassProccessor.extract({
              use: [
                {loader: "css-loader"},
                {loader: "sass-loader"},
                {loader: 'postcss-loader'}
              ]
            }) : [
              {loader: "style-loader"},
              {loader: "css-loader"},
              {
                loader: "postcss-loader",
                options: {
                  sourceMap: 'inline'
                }
              },
              {
                loader: "sass-loader",
                options: {
                  sourceMap: true,
                  includePaths: [
                    path.join(__dirname, 'src')
                  ]
                }
              }
            ],
        },
        {
          test: /\.js$/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['env']
              }
            }
          ],
          exclude: /node_modules/,
        }
      ]
    }
  }
}