import './app.scss';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/fromPromise';
import Chart  from 'chart.js';


const TypeAheadEl = document.querySelector('input[name="typeahead"]');
const list = document.querySelector('.suggestions');
const title = document.querySelector('#title');
const chart = document.getElementById('chart').getContext("2d");
const APIUrl = `https://api.github.com/users/`;
const myChart = new Chart(chart, {
  type: 'bar',
  options: {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: { beginAtZero: true}
      }],
      xAxes: [{
        ticks: {
          fontSize: 18
        }
      }]
    },
    legend: {
      labels: {
        fontSize: 15
      }
   }
  }
});

//Initialization of elements
TypeAheadEl.value = '';
list.style.display = 'none';

const user$ = Observable.fromEvent(TypeAheadEl, 'keyup')
  .map(({target}) => target.value)
  .do(value =>  list.style.display = value ? 'block' : 'none') //Side effect;
  .filter(value => value) // Only trigger request when the input has value
  .debounceTime(500);//Just so it does not trigger too much requests

const repos$ = user$
  .switchMap(user => fetch(`${APIUrl}${user}/repos`))
  .switchMap(response => response.json())

const click$ = Observable.fromEvent(document.querySelector('ul'), 'click')
  .map(({target}) => ([target.getAttribute('contributors-url'), target.getAttribute('repo-name')]))
  .filter(([url, name]) => url)
  .do(([url, name]) => {
    list.style.display = 'none';
    title.innerHTML = `
      Showing Contribution on repository: ${name}
    `;
  })
  .switchMap(([url]) => fetch(url))
  .switchMap(response => response.json())
  .map(contributions => contributions.map(({login, contributions}) => ({name: login, numOfContributions: contributions})))
  .subscribe(contributions => {
    const labels = contributions.map(cont => cont.name);
    const data = Object.assign({}, {
      label: `# of contributions per user${labels.length > 1 ? 's' : ''}`,
      data: contributions.map(contribution => contribution.numOfContributions),
      backgroundColor: contributions.map(contribution => random_rgba()),
      borderColor: contributions.map(contribution => random_rgba(true)),
    });
    const DATA = Object.assign({}, {datasets: [data]}, {labels});
    myChart.data = DATA;
    myChart.update();
  });

const blur$ = Observable.fromEvent(TypeAheadEl, 'focus')
  .filter(() =>  list.style.display === 'none' && list.children.length > 0)
  .subscribe(event => {
    list.style.display = 'block';
  });

repos$.subscribe(repos => {
  if (repos.message) {
    list.className = 'suggestions error';
    list.innerHTML = `
      <li class="error">User ${repos.message}</li>
    `;
  } else if (repos.length > 0) {
    list.className = 'suggestions';
    list.innerHTML = repos.map(r => `
      <li class="clickable" contributors-url="${r.contributors_url}" repo-name="${r.name}">${r.name}</li>
    `).join('');
  }
  else{
    list.className = 'suggestions warn';
    list.innerHTML = `
      <li class="warn">
        The user does not have repositories
      </li>
    `;
  }
});

function random_rgba(whitOpacity = false) {
  var o = Math.round, r = Math.random, s = 255;
  var op = whitOpacity ? 1 : r().toFixed(1);
  return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + op + ')';
}

